// ***********
// Author Troy Franks (outlaws42)
// Version 2024-10-27

// ************

// ********************* Settings
/* [Model Resolution (All dimensions are in Millimeters)] */
$fn = 256;

/* [Main Plate Properties] */

Length = 80.264; // 80.264 3.160
Width = 81.026;  // 81.026  3.190
Height = 3.048;

// ********************* Main Code

points = [ [ 0, 0 ], [ 100, 0 ], [ 130, 50 ], [ 30, 50 ] ];
polygon(points);

// ******************** End Main Code

// **************************** Modules Section

// ********************* End Modules
