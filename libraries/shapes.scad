module Triangle(adjacent_len, opposite_len, depth, x_deg = 0, y_deg = 0, z_deg = 0, center = false, )
{
    // Creates a right triangle then extrudes
    // You can also rotate about an axis.
    centerd = center ? [ -adjacent_len / 3, -opposite_len / 3, -depth / 2 ] : [ 0, 0, 0 ];
    translate(centerd) rotate([ x_deg, y_deg, z_deg ]) linear_extrude(height = depth)
        polygon(points = [ [ 0, 0 ], [ adjacent_len, 0 ], [ 0, opposite_len ] ], paths = [[ 0, 1, 2 ]]);
};
