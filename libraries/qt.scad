module quater_turn(diameter, height = 12.7, channel_size = 5, lock_distance = 10, lock_variant = .2032,
                   channel_postion = 0, drop_angle = 15, channel_length = 90, clean_up = .1, channels = 2, )
{
    // Addeds the quarter turn connection to primative that it is applied
    increment = 360 / channels;
    difference()
    {
        // Main Body
        children();
        for (i = [0:increment:360])
        {
            // Channel logic
            rotate(i + lock_distance) rotate_extrude(angle = channel_length - lock_distance, convexity = 10)
                translate([ ((diameter / 2) + clean_up) - channel_size, channel_postion ])
                    square(channel_size, center = false);
            rotate(i) rotate_extrude(angle = channel_length, convexity = 10)
                translate([ ((diameter / 2) + clean_up) - channel_size, channel_postion ])
                    square([ channel_size, channel_size - lock_variant ], center = false);
        };

        for (i = [channel_length:increment:360])
            //  Enter point logic
            rotate((i)-drop_angle) rotate_extrude(angle = drop_angle)
                translate([ ((diameter / 2) + clean_up) - channel_size, channel_postion ])
                    square([ channel_size, height + clean_up ], center = false);
    };
};

module base_markings(diameter, height = 12.7, drop_angle = 15, reveal = .5, channel_length = 90, channels = 2, )
{
    // Puts the indicators on the base for the entry points of the quarter turn
    increment = 360 / channels;
    children();
    for (i = [channel_length:increment:360])
        rotate((i)-drop_angle) rotate_extrude(angle = drop_angle) translate([
            diameter / 2,
            0,
        ])
            square(
                [
                    reveal,
                    height,
                ],
                center = false);
};
