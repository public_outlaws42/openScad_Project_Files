module Copy_Mirror(mirror_vector)
{
    // Makes a mirror copy of object.
    // Good if you want multiples of something.
    // Can even do diffence. Requires mirror_vector like [x,y,z]
    children();
    mirror(mirror_vector) children();
};
