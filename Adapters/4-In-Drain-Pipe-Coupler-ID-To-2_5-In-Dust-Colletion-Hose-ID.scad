// All units are in Milimeter
$fn = 256;

//Attachment Bottom - (4" Drain Fitting ID)
Bottom_OD_Small = 106.68; // 4.200"
Bottom_OD_Big = 107.696; // 4.240"
Bottom_Taper_Distance = 25.4; // 1.0"
Bottom_Wall_Thickness = 2.54; // .100
Bottom_Straight_Length = 25.4; // 1.0"
Bottom_Fit_OD_Big = .254; // .01"
Bottom_Fit_OD_Small = .254; // .01"
Transition_Length = 12.7;

//Attachment Top(2.5" Dust collection ID)
Top_OD_Small = 63.5; // 2.4"
Top_OD_Big = 63.5; // 2.5"
Top_Taper_Distance = 39.751; // 1.565
Top_Wall_Thickness = 2.54; // .100
Top_Straight_Length = 6.35; // .250"
Top_Fit_OD_Big = .254; // .01"
Top_Fit_OD_Small = .254; // .01"

// DON"T CHANGE (OD plus the fit added)
Top_OD_Big_Fit = Top_OD_Big+Top_Fit_OD_Big; 
Top_OD_Small_Fit = Top_OD_Small+Top_Fit_OD_Small;
Bottom_OD_Big_Fit = Bottom_OD_Big+Bottom_Fit_OD_Big; 
Bottom_OD_Small_Fit = Bottom_OD_Small+Bottom_Fit_OD_Small;  

// Extra for ID extension
ID_Extension_Lt = 5; 

//OD Profile
difference() { 
 Adapter_OD(
   Bottom_Taper_Distance,
   Bottom_OD_Small_Fit,
   Bottom_OD_Big_Fit,
   Bottom_Straight_Length,
   Transition_Length,
   Top_Taper_Distance,
   Top_OD_Small_Fit,
   Top_OD_Big_Fit,
   Top_Straight_Length,
   false
  );
  // Inside Profile
  Adapter_ID(
   Bottom_Taper_Distance,
   Bottom_OD_Small_Fit,
   Bottom_OD_Big_Fit,
   Bottom_Straight_Length,
   Transition_Length,
   Top_Taper_Distance,
   Top_OD_Small_Fit,
   Top_OD_Big_Fit,
    false
  );
};


module Adapter_OD(
  Taper_Distance_Bottom,
  OD_Small_Bottom,
  OD_Big_Bottom,
  Straight_Length_Bottom,
  Length_Of_Transition,
  Taper_Distance_Top,
  OD_Small_Top,
  OD_Big_Top,
  Straight_Length_Top,
  cntr=false
){
  // OD taper section to fit attachment
  cylinder(
   Taper_Distance_Bottom,
   d1=OD_Small_Bottom,
   d2=OD_Big_Bottom,
   center=cntr
  );
  // Straight section at OD
  translate([0,0,Taper_Distance_Bottom])
  cylinder(
   Straight_Length_Bottom,
   d=OD_Big_Bottom,
   center=cntr
  );
  // Taper section to OD for fit CS
  translate([0,0,Taper_Distance_Bottom+Straight_Length_Bottom])
  cylinder(
   Length_Of_Transition,
   d1=OD_Big_Bottom,
   d2=OD_Big_Top,
   center=cntr
  );
  // Straight section at OD
  translate([0,0,Taper_Distance_Bottom+Straight_Length_Bottom+Length_Of_Transition])
  cylinder(
   Straight_Length_Top,
   d=OD_Big_Top,
   center=cntr
  );
  // OD taper section to fit Sweeper
  translate([0,0,Taper_Distance_Bottom+Straight_Length_Bottom+Length_Of_Transition+Straight_Length_Top])
  cylinder(
   Taper_Distance_Top-Straight_Length_Top,
   d1=OD_Big_Top,
   d2=OD_Small_Top,
   center=cntr
  );

};

module Adapter_ID(
  Bottom_Taper_Distance,
  OD_Small_Bottom,
  OD_Big_Bottom,
  Bottom_Straight_Length,
  Transition_Length,
  Taper_Distance_Top,
  OD_Small_Top,
  OD_Big_Top,
  cntr=false
){
  // ID Section Start
  // ID taper section to fit fitting
  translate([0,0,-ID_Extension_Lt])
  cylinder(
   Bottom_Taper_Distance+ID_Extension_Lt+.05,
   d=OD_Big_Bottom-(Bottom_Wall_Thickness*2),
   center=false
  );
  // Straight section between ends
  translate([0,0,Bottom_Taper_Distance])
  cylinder(
   Bottom_Straight_Length+.05,
   d=OD_Big_Bottom-(Bottom_Wall_Thickness*2),
   center=false
  );
  // Taper section to OD for fit ID
  translate([0,0,Bottom_Taper_Distance+Bottom_Straight_Length])
  cylinder(
   Transition_Length,
   d1=OD_Big_Bottom-(Bottom_Wall_Thickness*2),
   d2=OD_Big_Top-(Top_Wall_Thickness*2),
   center=false
  );
  // Straight to oal
  translate([0,0,Bottom_Taper_Distance+Bottom_Straight_Length+Transition_Length-.1])
  cylinder(
   Taper_Distance_Top+ID_Extension_Lt,
   d=OD_Big_Top-(Top_Wall_Thickness*2),
   center=false
  );

};