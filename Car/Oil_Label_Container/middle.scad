include <var.scad>

/* [Model Resolution (All dimensions are in Millimeters)] */
$fn = 256;

// Tabs
increment = 360 / 2;
for (i = [0:increment:360])
{
    rotate(i) Tab(width = Tab_Catch, depth = Tab_Depth, height = Tab_Height, catch = Tab_Catch);
};

// Outside Dimensions
difference()
{
    cylinder(h = Middle_Height_Plus, d = (Middle_Inside_Diameter + (Wall_Thickness * 2)), center = false);
    // Inside Dimensions
    translate([ 0, 0, -1 ]) cylinder(Middle_Height_Plus + 3, d = Middle_Inside_Diameter, center = false);
};

module Tab(width, depth, height, catch)
{
    translate([ ((Middle_Inside_Diameter) / 2) - catch, -(width / 2), 0 ])

        cube([ depth, width, height ], center = false);
};
