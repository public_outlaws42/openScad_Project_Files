include<var.scad>

/* [Model Resolution (All dimensions are in Millimeters)] */
$fn = 256;
rounded();
difference()
{
    cylinder(h = Lid_Height, d = (Middle_Inside_Diameter + (Wall_Thickness * 2)), center = false);
    // ID of lid for the bottom part
    translate([ 0, 0, Wall_Thickness ])
        cylinder(h = Lid_Height + Lid_Inside_Height, d = Middle_Inside_Diameter - Lid_Fit - (Wall_Thickness * 2),
                 center = false);
};
difference()
{
    translate([ 0, 0, Lid_Height ])
        cylinder(h = Lid_Inside_Height, d = Middle_Inside_Diameter - Lid_Fit, center = false);
    // ID of lid for the top part
    translate([ 0, 0, Lid_Height - 1 ]) cylinder(
        h = Lid_Inside_Height + 2, d = Middle_Inside_Diameter - Lid_Fit - (Wall_Thickness * 2), center = false);
};
module rounded(radius_amount = 2.5)
{
    hull()
    {
        rotate_extrude(angle = 360, convexity = 2) translate([ Middle_Inside_Diameter / 2, 0 ]) circle(radius_amount);
    };
}
