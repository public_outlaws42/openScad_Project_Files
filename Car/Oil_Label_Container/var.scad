/* [Model Resolution (All dimensions are in Millimeters)] */
$fn = 256;

// Roll Dia 2.500"
// Roll Height 2.170"
// Roll ID 1.180"

/* [Padding] */
Middle_Fit = .508; // .020"
Lid_Fit = .254;    // .020

/* [Base Properties] */
Base_Bottom_Height = 12.7; // 1
Base_Top_Height = 12.2;    // 1
Base_Top_Dia = 63.5;       // 2.5
Post_Height = 55.118;      // 2.170
Post_Dia = 29.21;          // 1.150

/* [Middle Properties (Inside Diameter)] */
Middle_Height = 69.85;
Wall_Thickness = 2.54;

Middle_Height_Plus = Middle_Height + Base_Top_Height;

/*[Lid Properties] */
Lid_Height = 12.7;         // .500
Lid_Inside_Height = 9.525; // .375

/* [Tab Properties] */
Tab_Width = 4;
Tab_Height = 4.35;
Tab_Catch = Tab_Height - .35;
Tab_Reveal = .5;
Tab_Depth = Tab_Catch + Wall_Thickness + Tab_Reveal;

Channel_Depth = Tab_Catch + 1.0;

/* [Hidden] */
// Size Adjustments
Base_Dia_Padding = ((Channel_Depth * 2) + 1); // .200
Base_Top_Dia_Plus = Base_Top_Dia + Base_Dia_Padding;
Middle_Inside_Diameter = Base_Top_Dia_Plus + Middle_Fit;
