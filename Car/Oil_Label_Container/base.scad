include <var.scad>
use <qt.scad>

/* [Model Resolution (All dimensions are in Millimeters)] */
$fn = 256;

// Base Bottom
base_markings(diameter = (Middle_Inside_Diameter + (Wall_Thickness * 2)), height = Base_Bottom_Height)
    cylinder(h = Base_Bottom_Height, d = (Middle_Inside_Diameter + (Wall_Thickness * 2)), center = false);

// Base Top
translate([ 0, 0, Base_Bottom_Height ])
    quater_turn(diameter = Base_Top_Dia_Plus, height = Base_Top_Height, channel_size = Channel_Depth)

        cylinder(h = Base_Top_Height, d = (Base_Top_Dia_Plus), center = false);

// Base Post
translate([ 0, 0, Base_Bottom_Height + Base_Top_Height - 2.5 ]) cylinder(h = Post_Height, d = Post_Dia, center = false);
