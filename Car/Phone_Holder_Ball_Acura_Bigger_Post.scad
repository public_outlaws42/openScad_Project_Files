/* [Model Resolution (All dimensions are in Millimeters)] */
$fn = 200;

// Version = 2023-04-08

/* [Sphere Properties] */
sphere_dia = 16.891; // .665"

/* [Post Properties Properties] */
post_dia = 12.7; // .500"
post_length = 5; // .197

/* [Base Properties] */
base_dia = 12.7;    // .500"
base_length = 8;    // .315

/* [Hole Properties] */
hole_dia = 3.251;    // .128"
hole_depth = 3.988;    // .157

/* [Hidden] */
sphere_post_adjust = 3;
flat_height = 5;
flat_adjust = 2;

// Sphere
difference()
{
    sphere(d = sphere_dia);
    translate([ 0, 0, -(flat_height / 2 + sphere_dia / 2 - flat_adjust) ]) cube([ 20, 20, flat_height ], center = true);
}
// Post
translate([ 0, 0, sphere_dia / 2 - sphere_post_adjust ]) cylinder(h = post_length, d = post_dia);
difference(){
// Base
translate([ 0, 0, sphere_dia / 2 + post_length - sphere_post_adjust ])
    cylinder(h = base_length, d1 = post_dia, d2 = base_dia);

// Hole
translate([ 0, 0, sphere_dia / 2 + post_length+ base_length - sphere_post_adjust-hole_depth ])
    cylinder(h = base_length, d1 = hole_dia, d2 = hole_dia);
}
