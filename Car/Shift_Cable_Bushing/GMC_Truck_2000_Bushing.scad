// ***** Post Dimensions *******
// Small Dia = .175"
// Big Dia = .310"
// Groove Width = .075"
// Groove Dia = .197"
// Groove To End = .210"
// OAL = .410"

// ****** Cable End Dimensions ********
// ID = .315"
// End Thickness = .400"


/* [Model Resolution (All dimensions are in Millimeters)] */
$fn = 256;


/* [Cylinder Properties (Inside Diameter)] */
Diameter = 76.2;
Height = 10.414;
//Groove_Location = 5.08; // .200"
Groove_Width = 1.854; // .073"
Groove_Diameter = 5.004; //.197"
Wall_Thickness = 2.54;
Floor_Thickness = 6.3;
Big_Diameter_ID = 7.874; // .310
Small_Diameter_ID = 4.521; // .178
Big_End_Length = 5.08; // .200 To center of groove 
Small_End_Length = 5.334; // .210 To center of groove
Extra_End_Length = 12.7; // .500
Big_End_Length_Top = Big_End_Length-(Groove_Width/2); 
Small_End_Length_Top = Small_End_Length-(Groove_Width/2);
OAL = Big_End_Length+Small_End_Length;
difference(){
translate([0,0,.1])
cylinder(
h=OAL+Extra_End_Length,
d1=8.001,
d2=7.9502,
center=false
);

// Big End ID Taper
cylinder(
   h=Big_End_Length_Top,
   d1=Big_Diameter_ID,
   d2=(5.334), // .210
   center=false
  );

// Small End ID Taper
translate([0,0,Big_End_Length_Top+Groove_Width])
cylinder(
   h=Small_End_Length_Top,
   d1=5.334,
   d2=Small_Diameter_ID, // .178
   center=false
  );

// ID Dia At Groove.
%cylinder(
   h=Big_End_Length+Groove_Width,
   d=Groove_Diameter,
   center=false
  );
  
// ID Through
%cylinder(
   h=OAL+.5,
   d=Small_Diameter_ID,
   center=false
  );


};



