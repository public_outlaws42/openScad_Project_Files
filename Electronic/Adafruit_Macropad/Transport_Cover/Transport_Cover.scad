use <roundedcube.scad>;
/* [Model Resolution (All dimensions are in Millimeters)] */
$fn = 256;

/* [Box Container Properties(Inside Diameter)] */

Length = 110.236;            // 4.340"
Width = 66.294;              // 2.610"
Fit = 0;                     //.889(.035") is a good fit for most
Height = 35.052;             // 1.380"
Wall_Thickness = 2.54;       // .100"
Floor_Thickness = 2.54;      // .100"
Finger_Cutout_Depth = 10.16; // .400"
Finger_Cutout_Width = 22.86; // .900"
Corner_Radius = 6.35;        // .250"

difference()
{
    // Outside Dimensions
    roundedcube([ Length + Fit + (Wall_Thickness * 2), Width + Fit + (Wall_Thickness * 2), Height ],
                radius = Corner_Radius, apply_to = "z", center = false);
    // Inside Dimensions
    translate([ Wall_Thickness, Wall_Thickness, Floor_Thickness ])
        roundedcube([ Length + Fit, Width + Fit, Height ], radius = Corner_Radius, apply_to = "z", center = false);
    // Finger cutout to remove macropad
    translate([ (((Length / 2) + Wall_Thickness) - (Finger_Cutout_Width / 2)), -2, Height - Finger_Cutout_Depth ])
        cube([ Finger_Cutout_Width, Width + (Wall_Thickness * 2) + 5, Finger_Cutout_Depth + 2 ], center = false);
};
