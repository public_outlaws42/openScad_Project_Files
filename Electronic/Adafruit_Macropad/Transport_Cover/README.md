# Adafruit Macropad Transport Cover

![Transport Case](Static/Transport_Case.jpg)

## Intended Use.

The transport cover is made to fit over the slim version of this [macropad case](https://www.printables.com/model/138045-adafruit-macropad-case). It will protect the keys and works of the macropad. It is inspired by this [Cover](https://www.printables.com/model/612936-adafruit-macropad-rp2040-travel-lid-and-stand). My version doesn't have a stand builtin stand because I have a seperate [slanted stand](https://gitlab.com/public_outlaws42/openScad_Project_Files/-/tree/main/Electronic/Adafruit_Macropad/Angle_Stand?ref_type=heads). I have stand at home and at work and use the cover to transport.

## Required libraries

The openScad file requires

- roundcube.scad

You can get it from [here](https://danielupshaw.com/openscad-rounded-corners/)

You would put this file in the libraries dir in openSCAD. On Linux this would be located at `~/.config/OpenSCAD/libraries`
