# Adafruit Macropad Stand

![Transport Case](Static/Stand.jpg)

## Intended Use.

The Angled stand is made to be used with the slim version of this [macropad case](https://www.printables.com/model/138045-adafruit-macropad-case). The macropad will just sit on this stand.
It holds the macropad at angle so you can see the display better. It is crude looking but it works well.

## Required Libraries

The openSCAD file requires these libraries.

- shapes.scad
- operations.scad

They can be found in my [libraries](https://gitlab.com/public_outlaws42/openScad_Project_Files/-/tree/main/libraries)

You would put this file in the libraries dir in openSCAD. On Linux this would be located at `~/.config/OpenSCAD/libraries`
