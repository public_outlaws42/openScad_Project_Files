// ***********
// Author Troy Franks (outlaws42)
// Version 2024-10-31

// ************

// ********************* Settings
/* [Model Resolution (All dimensions are in Millimeters)] */
$fn = 256;

/* [Side Stop Properties] */
Front_Side_Stop_Width = 15;
Side_Stop_Thickness = 2;
Front_Side_Stop_Height = 10;

// ********************* Main Code

// Side Stop
cube([ Front_Side_Stop_Width, Side_Stop_Thickness, Front_Side_Stop_Height ]);
