// ***********
// Author Troy Franks (outlaws42)
// Version 2024-10-31

// ************

// ********************* Settings
/* [Model Resolution (All dimensions are in Millimeters)] */
$fn = 256;

/* [Side Stop Properties] */
Back_Side_Stop_Width = 10;
Side_Stop_Thickness = 2;
Back_Side_Stop_Height = 28.5;

// Side Stop

cube([ Back_Side_Stop_Width, Side_Stop_Thickness, Back_Side_Stop_Height ]);
