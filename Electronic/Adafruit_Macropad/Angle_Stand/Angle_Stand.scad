// Requires the Triangle module from shapes.scad
// Requires the Copy_Mirror module from operations.scad
// These can be found at https://gitlab.com/public_outlaws42/openScad_Project_Files/-/tree/main/libraries 
include <operations.scad>
include <shapes.scad>


// ***********
// Author Troy Franks (outlaws42)
// Version 2024-10-31

// ************

// ********************* Settings
/* [Model Resolution (All dimensions are in Millimeters)] */
$fn = 256;

/* [Base Plate Properties] */

Length = 127;   // 5"
Width = 66.675; // 2.625"
Height = 3.175; // .125"
Corner_Radius = 7.62;

/* [Angle Properties] */
Angle_Length = 114.3;
Angle_Height = 30;
Angle_Thickness = 6.35;

/* [End Stop Properties] */
Stop_Thickness = 3.175; // .125"
Stop_Height = 10;       // .3937"

/* [Side Stop Properties] */
Front_Side_Stop_Width = 15;
Back_Side_Stop_Width = 10;
Side_Stop_Thickness = 2;
Back_Side_Stop_Height = 28.5;
Front_Side_Stop_Height = 10;
Back_Side_Stop_Location = 22.4;

/* [Bolt Cutout Properties]*/
Back_Cutout_Distance = 3.175;
Back_Cutout_Width = 12.7;
Front_Cutout_Width = 12.7;
Front_Cutout_Distance = 78.74; // 3.1

// ********************* Main Code

// Plate_Properties();

// Triangle Code
difference()
{
    translate([ Corner_Radius, Width / 2, Height ]) Copy_Mirror([ 0, 10, 0 ]) translate([ 0, Width / 2, 0 ])
        Triangle(Angle_Length, Angle_Height, Angle_Thickness, x_deg = 90);
    // Cutout Back
    translate([ Corner_Radius, (Width / 2) + 2, 0 ]) Copy_Mirror([ 0, 10, 0 ])
        translate([ Back_Cutout_Distance, 0, Angle_Height - 1.5 ]) cube([ Back_Cutout_Width, Width, 10 ]);
    // Cutout Front
    translate([ Corner_Radius, (Width / 2) + 2, 0 ]) Copy_Mirror([ 0, 10, 0 ])
        translate([ Front_Cutout_Distance, 0, 7.5 ]) cube([ Front_Cutout_Width, Width, 5 ]);
};

// Corner Radius On Base
translate([ Corner_Radius, Corner_Radius, 0 ]) cuberad(Length, Width, Height, Corner_Radius * 2, "corner");

// Side Stops Back
translate([ Corner_Radius + Back_Side_Stop_Location, Width / 2, 0 ]) Copy_Mirror([ 0, 10, 0 ])
    translate([ 0, Width / 2, 0 ]) cube([ Back_Side_Stop_Width, Side_Stop_Thickness, Back_Side_Stop_Height ]);

// Side Stops Front
translate([ Length - Corner_Radius - Front_Side_Stop_Width, Width / 2, 0 ]) Copy_Mirror([ 0, 10, 0 ])

    translate([ 0, Width / 2, 0 ]) cube([ Front_Side_Stop_Width, Side_Stop_Thickness, Front_Side_Stop_Height ]);

// Stop
translate([ Length - Stop_Thickness - Corner_Radius, 0, 0 ]) cube([ Stop_Thickness, Width, Stop_Height ]);

// ******************** End Main Code

// **************************** Modules Section

module cuberad(xsize = 10, ysize = 10, zsize = 10, diameter = 2, type = "all")
{
    // Example cuberad
    // cuberad(width,length,height,diameter of radius)
    if (type == "all")
    {
        minkowski()
        {
            cube([ xsize - diameter / 2, ysize - diameter / 2, zsize - diameter / 2 ], center = false);
            sphere(d = diameter);
        }
    }
    else if (type == "corner")
    {
        minkowski()
        {
            cube([ xsize - diameter, ysize - diameter, zsize ], center = false);
            cylinder(d = diameter);
        }
    }
}

// ********************* End Modules
