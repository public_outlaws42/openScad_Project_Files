/* [Model Resolution (All dimensions are in Millimeters)] */
$fn = 256;

/* [Cylinder Properties (Inside Diameter)] */
Diameter = 25.4; // 1.0"
Height = 0;
Wall_Thickness = 0;
Floor_Thickness = 7.62; // .300"

/* [Tab Properties] */
Shim_Thickness = 4.572; // .180"
Tab_Width = 12.7; // .500"
Tab_Thickness = 4.318; // .170"
Tab_From_Edge = 2.794; // .110"


/* [Screw Hole Properties] */
Screw_Hole_On = false;
Screw_Hole_Diameter = 5.08;
Chamfer_Adjust = 3.5; 

// Outside Dimensions
difference() { 
 cylinder(
   h=Height+Floor_Thickness,
   d=(Diameter+(Wall_Thickness*2)),
   center=false
  );
  // Inside Dimensions
  translate([0,0,Floor_Thickness])
  cylinder(
   Height+Floor_Thickness,
   d=Diameter,
   center=false
  );
  // Shim Tab
  translate([(Diameter/2)-Tab_Thickness-Tab_From_Edge,-Tab_Width/2,Shim_Thickness])
  cube([
   Tab_Thickness,
   Tab_Width,
   10,
   ],
   center=false
  );
  if(Screw_Hole_On==true) {
    // Screw Hole
    translate([0,0,-Floor_Thickness])
    cylinder(
      h=Floor_Thickness+30,
      d=Screw_Hole_Diameter,
      center=false
    );
    // Chamfer
    translate([0,0,Floor_Thickness-Chamfer_Adjust])
    cylinder(
      h=Screw_Hole_Diameter,
      d1=Screw_Hole_Diameter,
      d2=Screw_Hole_Diameter*2,
      center=false
    );

  }
  
};


