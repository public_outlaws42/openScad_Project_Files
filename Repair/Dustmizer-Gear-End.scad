/* [Model Resolution (All dimensions are in Millimeters)] */
$fn = 256;

/* [Cylinder Properties (Outside Diameter)] */
Diameter = 7.874; // .310"
Length = 7.315;   //.288

/* [Inside Diameter Properties] */
ID = 2.667; // .105

/* [Hex Properties] */
Hex_Size = 4;      // .1575
Hex_Depth = 4;     // .1575
Hex_Translate = 1; // .0394

difference()
{
    cylinder(h = Length, d = Diameter, center = false);
    translate([ 0, 0, -2 ])
    {
        cylinder(h = Length + 4, d = ID, center = false);
    }
    id_hex(hex_size = Hex_Size, depth = Hex_Depth, cleanup = Hex_Translate);
    // 45 deg chamfer on hex
    translate([ 0, 0, -Hex_Translate ]) cylinder(h = 3.5, d1 = 7, d2 = 0, center = false, $fn = 256);
}

module id_hex(hex_size, depth, cleanup)
{
    hex_dia_diff = 1; //.0394
    difference()
    {
        translate([ 0, 0, -cleanup ])
        {
            cylinder(h = depth + cleanup, d = hex_size + hex_dia_diff, center = false, $fn = 6);
        }
    }
}
